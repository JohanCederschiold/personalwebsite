import Vue from 'vue'
import VueRouter from 'vue-router'
import Who from '../views/WhoIAm.vue'
import Competence from '../views/Competence.vue'
import Contact from '../views/Contact.vue'
import Portfolio from '../views/Portfolio.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'who',
    component: Who
  }, 
  {
    path: '/resume',
    name: 'resume',
    component: Competence
  },
  {
    path: '/contact',
    name: 'contact',
    component: Contact
  },
  {
    path: '/portfolio',
    name: 'portfolio',
    component: Portfolio
  }
]

const router = new VueRouter({
  routes
})

export default router
